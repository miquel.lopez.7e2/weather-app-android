package com.itb.weatheralarm;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("com.example.weatherappandroid", appContext.getPackageName());
    }


    @Test
    public void testingLogin() {

        onView(withId(R.id.ed_login_email)).perform(replaceText("test2@gmail.com"));
        onView(withId(R.id.ed_login_password)).perform(replaceText("test2345"), closeSoftKeyboard());
        onView(withId(R.id.btn_login_login)).perform(click());
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.fragment_alarmlist)).check(matches(isDisplayed()));
    }

    @Test
    public void testingRegister() {
        onView(withId(R.id.btn_login_register)).perform(click());
        onView(withId(R.id.fragment_register)).check(matches(isDisplayed()));
        onView(withId(R.id.ed_register_email)).perform(replaceText("test4@gmail.com"));
        onView(withId(R.id.ed_register_password)).perform(replaceText("test4567"));
        onView(withId(R.id.ed_register_password_repeat)).perform(replaceText("test4567"), closeSoftKeyboard());
        onView(withId(R.id.btn_register_register)).perform(click());
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.fragment_alarmlist)).check(matches(isDisplayed()));

    }

}
