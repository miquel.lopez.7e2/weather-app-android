package com.itb.weatheralarm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import com.itb.weatheralarm.mathquizgame.model.Game;
import com.itb.weatheralarm.service.AlarmService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.ALARMID;


public class AlarmRing extends AppCompatActivity {


    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.tv_question)
    TextView tvQuestion;
    private int[] answer;
    private Window wind;
    private int alarmId, postpone;
    Intent intentService;

    private AlarmRingViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wind = this.getWindow();
        mViewModel = ViewModelProviders.of(this).get(AlarmRingViewModel.class);

        wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        wind.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        wind.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_alarm_ring);
        ButterKnife.bind(this);
        intentService = new Intent(getApplicationContext(), AlarmService.class);

        Intent alarmService = getIntent();
        alarmId = alarmService.getIntExtra(ALARMID, 0);

        mViewModel.createGame();
        MutableLiveData<Game> game = mViewModel.getGameMutableLiveData();
        game.observe(this, this::onGameChanged);

    }

    private void onGameChanged(Game game) {
        display(game);
    }

    private void display(Game game) {
        answer = game.getCurrentQuestion().getAnswersArray();

        button.setText(Integer.toString(answer[0]));
        button2.setText(Integer.toString(answer[1]));
        button3.setText(Integer.toString(answer[2]));
        button4.setText(Integer.toString(answer[3]));

        tvQuestion.setText(game.getCurrentQuestion().getQuestionPhrase());
    }

    private void stopAlarmService() {
        getApplicationContext().stopService(intentService);
        finish();
    }

    private void correctAnswer(int answer){
        if (mViewModel.checkAnswer(answer)){
            stopAlarmService();
        } else {
            mViewModel.createGame();
        }
    }

    @OnClick({R.id.button, R.id.button2, R.id.button3, R.id.button4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button:
                correctAnswer(answer[0]);
                break;
            case R.id.button2:
                correctAnswer(answer[1]);
                break;
            case R.id.button3:
                correctAnswer(answer[2]);
                break;
            case R.id.button4:
                correctAnswer(answer[3]);
                break;
        }
    }
}
