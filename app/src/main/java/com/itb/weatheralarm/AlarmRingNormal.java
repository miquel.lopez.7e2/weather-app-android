package com.itb.weatheralarm;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.service.AlarmService;

import java.util.Calendar;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.itb.weatheralarm.MyValues.ALARM_TITLE;
import static com.itb.weatheralarm.MyValues.DEFAULT_POSTPONE_TIME;
import static com.itb.weatheralarm.MyValues.POSTPONE;
import static com.itb.weatheralarm.MyValues.TEMPERATURE;

public class AlarmRingNormal extends AppCompatActivity {

    private AlarmRingViewModel mViewModel;
    Intent intentService;
    private int postpone;

    @BindView(R.id.tvAlarmName)
    TextView tvTitle;

    @BindView(R.id.tvTemperature)
    TextView tvTemperature;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window wind = this.getWindow();
        mViewModel = ViewModelProviders.of(this).get(AlarmRingViewModel.class);

        wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        wind.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        wind.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_alarm_ring_normal);
        ButterKnife.bind(this);

        intentService = new Intent(getApplicationContext(), AlarmService.class);
        postpone = getIntent().getIntExtra(POSTPONE, DEFAULT_POSTPONE_TIME);
        String title = getIntent().getStringExtra(ALARM_TITLE);
        double temperature = getIntent().getDoubleExtra(TEMPERATURE, 0);
        System.out.println("TEMPERATURE IN ALARM RING " + temperature);

        tvTitle.setText(title);
        tvTemperature.setText(temperature + " ºC");

    }

    @OnClick(R.id.btn_alarmring_normal_turnoff)
    public void turnOffAlarm() {
        stopAlarmService();
    }

    @OnClick(R.id.btn_alarmring_normal_pospose)
    public void postponeAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MINUTE, postpone);

        int alarmId = new Random().nextInt(Integer.MAX_VALUE) * -1;

        Alarm alarm = new Alarm(
                alarmId,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                postpone,
                "Snooze",
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false
        );

        mViewModel.insertAlarm(alarm);
        alarm.scheduleAlarm(getApplicationContext());

        stopAlarmService();

    }

    private void stopAlarmService() {
        getApplicationContext().stopService(intentService);
        finish();
    }
}
