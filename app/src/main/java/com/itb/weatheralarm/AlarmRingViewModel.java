package com.itb.weatheralarm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.itb.weatheralarm.mathquizgame.model.Game;
import com.itb.weatheralarm.database.local.LocalDatabaseRepository;
import com.itb.weatheralarm.model.Alarm;

public class AlarmRingViewModel extends AndroidViewModel {

    private LocalDatabaseRepository repository;
    MutableLiveData<Game> gameMutableLiveData = new MutableLiveData<>();

    public AlarmRingViewModel(@NonNull Application application) {
        super(application);
        repository = new LocalDatabaseRepository(getApplication());
    }

    public LiveData<Alarm> getAlarm(int alarmId) {
        return repository.getAlarmByIDStream(alarmId);
    }

    public void insertAlarm(Alarm alarm) {
        repository.insert(alarm);
    }

    public MutableLiveData<Game> getGameMutableLiveData() {
        return gameMutableLiveData;
    }

    public void createGame() {
        Game game = new Game();
        game.newQuestion();
        gameMutableLiveData.postValue(game);
    }

    public boolean checkAnswer(int i) {
        return gameMutableLiveData.getValue().checkAnswer(i);
    }
}
