package com.itb.weatheralarm;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itb.weatheralarm.ui.alarmslist.AlarmsListFragment;
import com.itb.weatheralarm.ui.login.LoginFragment;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        //Bottom Navigation View
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        BottomAppBar bottomAppBar = findViewById(R.id.bottomAppBar);
        FloatingActionButton fab = findViewById(R.id.add_button);

        //Array de fragments
        Integer[] noAppBarFragmentsArray = {
                R.id.navigation_add_alarm,
                R.id.navigation_login,
                R.id.navigation_register,
                R.id.navigation_audio_list};
        Set<Integer> noToolbarFragments = new HashSet<Integer>(Arrays.asList(noAppBarFragmentsArray));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(R.id.navigation_add_alarm);
            }
        });

        // NavController
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (noToolbarFragments.contains(destination.getId())) {
                    bottomNavigationView.setVisibility(View.GONE);
                    bottomAppBar.setVisibility(View.GONE);
                    fab.setVisibility(View.GONE);

                } else {
                    bottomNavigationView.setVisibility(View.VISIBLE);
                    bottomAppBar.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.VISIBLE);
                }
            }
        });


        //App Bar Configuration
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph())
                .build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);

    }

    @Override
    public void onBackPressed() {
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        boolean handled = false;
        for (Fragment f : frags) {
            if (f instanceof AlarmsListFragment) {
                handled = ((AlarmsListFragment) f).onBackPressed();
            }

        }
        if (!handled) {
            super.onBackPressed();
        }
    }
}
