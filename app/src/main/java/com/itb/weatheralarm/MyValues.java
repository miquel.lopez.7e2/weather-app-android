package com.itb.weatheralarm;

public final class MyValues {

    public static final String SHARED_PREFERENCES = "parameters";
    public static final String RAIN_TIME_TEXT = "rainTime";
    public static final String VIBRATION = "VIBRATION";
    public static final String SNOW_TIME_TEXT = "snowTime";
    public static final String LOCATION = "location";
    public static final String POSTPONE = "POSTPONE";
    public static final String ALARM_TITLE = "TITLE";
    public static final String TEMPERATURE = "TEMPERATURE";
    public static final String DEFAULT_RING_TONE = "ring";
    public static final String ALARM_CHANNEL_ID = "MYALARM_CHANNEL";
    public static final int DEFAULT_RAIN_TIME = 10;
    public static final int DEFAULT_SNOW_TIME = 10;
    public static final int DEFAULT_CREATE_ID = -1;
    public static final int DEFAULT_POSTPONE_TIME = 10;
    public static final int DEFAULT_API_CALL_TIME = 30;
    public static final String DEFAULT_LOCATION = "Barcelona";
    public static final String TONE = "TONE";


}
