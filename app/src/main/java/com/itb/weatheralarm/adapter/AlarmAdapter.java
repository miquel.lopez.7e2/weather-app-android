package com.itb.weatheralarm.adapter;

import android.app.AlertDialog;
import android.app.Application;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.ui.AlarmsViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Classe que serveix per a fer el RecyclerView de la view principal, AlarmsList.
 */

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder> {

    List<Alarm> alarms;
    OnAlarmClickedListener listener;
    AlarmsViewModel mViewModel;
    OnToggleAlarmListener toggleListener;
    Application application;

    public AlarmAdapter(Application application) {
        mViewModel = new AlarmsViewModel(application);
        this.application = application;
    }


    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return new AlarmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder holder, int position) {


        Alarm alarm = alarms.get(position);

        String alarmText = String.format("%02d:%02d", alarm.getHour(), alarm.getMinute());

        if (alarm.getTitle() == null) {
            holder.name.setText("My Alarm");
        } else {
            holder.name.setText(alarm.getTitle());
        }

        holder.days.setText(alarm.getRecurringDaysText());

        holder.hour.setText(alarmText);

        holder.switchAlarm.setOnCheckedChangeListener(null);


        holder.switchAlarm.setChecked(alarm.isStarted());


        holder.switchAlarm.setOnCheckedChangeListener((buttonView, isChecked) -> toggleListener.onToggleChanged(alarm));

    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (alarms != null) {
            size = alarms.size();
        }
        return size;
    }

    public void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
        notifyDataSetChanged();
    }

    public void setAlarmListener(OnAlarmClickedListener listener) {
        this.listener = listener;
    }

    public void setToggleListener(OnToggleAlarmListener listener) {
        this.toggleListener = listener;
    }

    public class AlarmViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_item_alarmname)
        TextView name;

        @BindView(R.id.tv_item_hour)
        TextView hour;

        @BindView(R.id.tv_item_days)
        TextView days;

        @BindView(R.id.switch_item)
        Switch switchAlarm;

        public AlarmViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this::alarmClicked);
            itemView.setOnLongClickListener(this::deleteAlarm);

        }


        private boolean deleteAlarm(View view) {
            Alarm alarm = alarms.get(getAdapterPosition());
            new AlertDialog.Builder(view.getContext())
                    .setIcon(R.drawable.ic_delete_black_24dp)
                    .setTitle("Are you sure?")
                    .setMessage("Do you want to delete this alarm?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        if (alarm.isStarted()) {
                            alarm.cancelAlarm(application.getApplicationContext());
                        }

                        if (alarm.isApiServiceStarted()) {
                            alarm.cancelApiService(application.getApplicationContext());
                        }

                        mViewModel.deleteAlarm(alarm);
                    })
                    .setNegativeButton("No", null)
                    .show();
            return true;
        }

        private void alarmClicked(View view) {
            Alarm alarm = alarms.get(getAdapterPosition());
            listener.onAlarmClicked(alarm);
        }
    }

    public interface OnAlarmClickedListener {
        void onAlarmClicked(Alarm alarm);
    }

    public interface OnToggleAlarmListener {
        void onToggleChanged(Alarm alarm);
    }
}
