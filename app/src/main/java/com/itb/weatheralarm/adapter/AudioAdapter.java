package com.itb.weatheralarm.adapter;

import android.app.Activity;
import android.app.Application;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.ui.AlarmsViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.AudioViewHolder> {


    private List<String> songs = new ArrayList<String>();
    MediaPlayer mediaPlayer;
    Application application;
    AlarmsViewModel mViewModel;
    OnAudioClickListener listener;

    public void setListener(OnAudioClickListener listener) {
        this.listener = listener;
    }

    public AudioAdapter(Application application) {
        this.application = application;
        mViewModel = new AlarmsViewModel(application);
    }

    @NonNull
    @Override
    public AudioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.audio_item, parent, false);
        return new AudioViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AudioViewHolder holder, int position) {
        String string = songs.get(position);
        holder.tvAudioName.setText(string);
        int resourceID = application.getResources().getIdentifier(songs.get(position), "raw", application.getPackageName());
        holder.playButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                }
                mediaPlayer = MediaPlayer.create(application.getApplicationContext(), resourceID);
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (songs != null) {
            size = songs.size();
        }
        return size;
    }

    public void setSongs(List<String> songs) {
        this.songs = songs;
    }

    public class AudioViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_audio_name)
        TextView tvAudioName;
        @BindView(R.id.play_button)
        Button playButton;
        @BindView(R.id.audio_row)
        ConstraintLayout audioRow;

        public AudioViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            audioRow.setOnClickListener(this::rowClicked);
        }

        private void rowClicked(View view) {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
            }
            String string = songs.get(getAdapterPosition());
            listener.onRowClicked(string);
        }

    }

    public interface OnAudioClickListener{
        void onRowClicked(String string);
    }
}
