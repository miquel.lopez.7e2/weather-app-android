package com.itb.weatheralarm.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.ui.settings.AccountSettingsFragment;
import com.itb.weatheralarm.ui.settings.AlarmSettingsFragment;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public TabAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public TabAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AlarmSettingsFragment.newInstance(0,"Alarms");

            case 1:
                return AccountSettingsFragment.newInstance(1, "Account");
            default:
                return null;
        }
    }

    public void addFragment(Fragment fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Alarms";
            case 1:
                return "Account";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
