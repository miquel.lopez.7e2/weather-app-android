package com.itb.weatheralarm.api;

import com.itb.weatheralarm.model.weatherApi.Forecast;
import com.itb.weatheralarm.model.weatherApi.Main;

import java.io.IOException;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class WeatherRepository {
    private WeatherService service;
    private String KEY = "41f26a0fec19f40f83ee37a61d162d85";

    private static final String THUNDERSTORM = "Thunderstorm";
    private static final String DRIZZLE = "Drizzle";
    private static final String RAIN = "Rain";
    private static final String SNOW = "Snow";

    public WeatherRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.service = retrofit.create(WeatherService.class);
       /* StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);*/
    }


    public int getDelay(String location, int rainTime, int snowTime) {

        try {
            //Get the forecast from the city in parameters
            Forecast forecast = service.getForecast(location, KEY).execute().body();

           /* if (forecast == null) {
                System.out.println("FORECAST IS NULL");
            }*/

            switch (forecast.getWeather().get(0).getMain()) {
                case THUNDERSTORM:
                case DRIZZLE:
                case RAIN:
                    //return minutes from rain taken by parameter
                    System.out.println("RAIN " + rainTime);
                    return rainTime;
                //return minRain;
                case SNOW:
                    //return minutes from snow taken by parameter
                    System.out.println("SNOW " + snowTime);
                    return snowTime;
                default:
                    //return default if is not a case
                    System.out.println("DEFAULT");
                    return 0;
            }


        } catch (Exception e) {
            // TODO improve this (maybe no internet)
            // If error no delay for now
            System.out.println("CATCHING WITH 0");
            e.printStackTrace();
            return 0;
        }

    }

    public double getCurrentWeather(String location) {
        try {
            Forecast forecast = service.getMetricForecast(location,KEY,"metric").execute().body();

            return forecast.getMain().getTemp();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("CATCHING WITH 0");
            return 0;
        }
    }

}
