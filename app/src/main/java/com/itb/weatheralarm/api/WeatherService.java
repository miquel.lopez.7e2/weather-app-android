package com.itb.weatheralarm.api;

import com.itb.weatheralarm.model.weatherApi.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("weather")
    Call<Forecast> getForecast(@Query("q") String cityName, @Query("appid") String key);

    @GET("weather")
    Call<Forecast> getMetricForecast(@Query("q") String cityName, @Query("appid") String key, @Query("units") String units);
}
