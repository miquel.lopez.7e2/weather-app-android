

package com.itb.weatheralarm.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.itb.weatheralarm.service.ApiService;

public class ApiBroadcastReceiver extends BroadcastReceiver {

    public static final String ALARMID = "ALARMID";

    @Override
    public void onReceive(Context context, Intent intent) {
        startApiService(context, intent);
    }

    private void startApiService(Context context, Intent intent) {
        System.out.println("STARTING API SERVICE");
        Intent intentService = new Intent(context, ApiService.class);
        intentService.putExtra(ALARMID, intent.getIntExtra(ALARMID, 0));
        context.startService(intentService);
    }
}


