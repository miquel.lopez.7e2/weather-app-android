package com.itb.weatheralarm.database.firestore;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import com.itb.weatheralarm.model.User;

public class FirebaseAuthentication {

    FirebaseAuth auth;
    FirebaseUser user;
    FirestoreDatabase db;
    AuthCredential credential;


    public FirebaseAuthentication() {
        this.db = new FirestoreDatabase();
        this.auth = FirebaseAuth.getInstance();
        this.user = auth.getCurrentUser();

    }

    public void createUser(Context context, String emailStr, String passwordStr) {

        auth.createUserWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userInfo = new User("username", emailStr);
                    db.addUserReferenceToDB(userInfo);
                } else {
                    // If sign in fails, display a message to the user.
                    FirebaseAuthException e = (FirebaseAuthException) task.getException();
                    System.out.println(e);
                    Toast.makeText(context, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                System.out.println("FALLA");
            }
        });
    }

    public void changePassword(Context context, String oldPass, String newPass) {
        this.credential = EmailAuthProvider.getCredential(user.getEmail(), oldPass);
        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                                Toast.makeText(context, "Password changed succesfully", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(context, "Something went wrong, try again later", Toast.LENGTH_LONG).show();
                        }
                    });
                else
                    Toast.makeText(context, "Authentication failed, try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void updateEmail(Context context, String email, String password) {
        this.credential = EmailAuthProvider.getCredential(user.getEmail(), password);
        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    user.updateEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                                Toast.makeText(context, "Email changed succesfully", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(context, "Something went wrong, try again later", Toast.LENGTH_LONG).show();
                        }
                    });
                else
                    Toast.makeText(context, "Authentication failed, try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void restorePassword(String email) {
        auth.sendPasswordResetEmail(email);
    }

    public void signOut() {
        auth.signOut();
    }

}
