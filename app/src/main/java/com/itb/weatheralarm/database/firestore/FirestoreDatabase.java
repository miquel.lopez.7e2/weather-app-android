package com.itb.weatheralarm.database.firestore;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe que es connecta amb la base de dades de Firestore. Fa els mètodes CRUD agafant les dades de l'usuari que està utilitzant l'app
 */
public class FirestoreDatabase {

    FirebaseFirestore db;

    FirebaseAuth user;

    MutableLiveData<List<Alarm>> alarms = new MutableLiveData<>();
    ArrayList<Alarm> aux = new ArrayList<>();

    public FirestoreDatabase() {
        this.db = FirebaseFirestore.getInstance();
        this.user = FirebaseAuth.getInstance();
    }

    public void addUserReferenceToDB(User userInfo) {
        db.collection("user").document(user.getCurrentUser().getUid()).set(userInfo).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                System.out.println("Done");
            } else {
                FirebaseException e = (FirebaseException) task.getException();
                System.out.println(e);
            }
        });
    }

    public LiveData<List<Alarm>> getAlarmsFromDB() {

        db.collection("user").document(user.getCurrentUser().getUid()).collection("alarms").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    System.out.println("SE HA HECHO LA CALL");
                    for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                        Alarm alarm = documentSnapshot.toObject(Alarm.class);
                        aux.add(alarm);
                    }

                    alarms.postValue(aux);
                }
            }
        }).addOnFailureListener(e -> System.out.println("NO SE HACE LA CALL"));

        aux.clear();

        return alarms;

    }

    public boolean uploadAlarm(Alarm alarm) {
        final boolean[] uploaded = {false};
        db.collection("user")
                .document(user.getCurrentUser().getUid())
                .collection("alarms")
                .document(alarm.getAlarmId() + "").set(alarm)
                .addOnCompleteListener(task -> {
                    System.out.println("Uploaded alarm");
                    uploaded[0] = true;
                });

        return uploaded[0];
    }

    public boolean updateAlarm(Alarm alarm) {
        final boolean[] updated = {false};

        List<Boolean> repeatDays = new ArrayList<>();
        repeatDays.add(alarm.isMonday());
        repeatDays.add(alarm.isTuesday());
        repeatDays.add(alarm.isWednesday());
        repeatDays.add(alarm.isThursday());
        repeatDays.add(alarm.isFriday());
        repeatDays.add(alarm.isSaturday());
        repeatDays.add(alarm.isSunday());
        db.collection("user")
                .document(user.getCurrentUser().getUid())
                .collection("alarms")
                .document(alarm.getAlarmId() + "").update("hour", alarm.getHour(), "minute", alarm.getMinute(), "title", alarm.getTitle(),
                "recurring", alarm.isRecurring(), "repeatDays", repeatDays, "game", alarm.isGame())
                .addOnSuccessListener(aVoid -> {
                    System.out.println("Updated alarm");
                    updated[0] = true;
                });

        return updated[0];
    }

    public boolean deleteAlarm(Alarm alarm) {
        final boolean[] deleted = {false};
        db.collection("user")
                .document(user.getCurrentUser().getUid())
                .collection("alarms")
                .document(alarm.getAlarmId() + "").delete()
                .addOnSuccessListener(aVoid -> {
                    System.out.println("Deleted alarm");
                    deleted[0] = true;
                });
        return deleted[0];
    }
}
