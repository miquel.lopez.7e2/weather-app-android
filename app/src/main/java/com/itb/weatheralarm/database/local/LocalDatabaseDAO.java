package com.itb.weatheralarm.database.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.itb.weatheralarm.model.Alarm;

import java.util.List;

@Dao
public interface LocalDatabaseDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAlarm(Alarm alarm);

    @Query("DELETE FROM alarms")
    void deleteAllAlarms();

    @Query("SELECT * FROM alarms ORDER BY hour ASC, minute ASC")
    public LiveData<List<Alarm>> getAlarmsStream();

    @Query("SELECT * FROM alarms WHERE alarmId =:id")
    public LiveData<Alarm> getAlarmByIDStream(int id);

    @Query("SELECT * FROM alarms WHERE alarmId =:id")
    public Alarm getAlarmByID(int id);

    @Query("DELETE FROM alarms where alarmId=:id")
    void deleteAlarm(int id);

    @Query("DELETE FROM alarms")
    void deleteTable();

    @Update
    void updateAlarm(Alarm alarm);

}
