package com.itb.weatheralarm.database.local;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.itb.weatheralarm.model.Alarm;

import java.util.List;

public class LocalDatabaseRepository {

    private LocalDatabaseDAO alarmDao;

    private LiveData<List<Alarm>> alarmsLiveData;

    public LocalDatabaseRepository(Application application) {
        LocalDatabase db = LocalDatabase.getDatabase(application);
        alarmDao = db.alarmDao();
        alarmsLiveData = alarmDao.getAlarmsStream();
    }

    public void insert(Alarm alarm) {
        LocalDatabase.databaseWriteExecutor.execute(() -> {
            alarmDao.insertAlarm(alarm);
        });
    }

    public void update(Alarm alarm) {
        LocalDatabase.databaseWriteExecutor.execute(() -> {
            alarmDao.updateAlarm(alarm);
        });
    }

    public LiveData<List<Alarm>> getAlarms() {
        return alarmsLiveData;
    }

    public LiveData<Alarm> getAlarmByIDStream(int id) {
        return alarmDao.getAlarmByIDStream(id);
    }

    public Alarm getAlarmByID(int id) {
        return alarmDao.getAlarmByID(id);
    }

    public void deleteAlarm(int id) {
        LocalDatabase.databaseWriteExecutor.execute(() -> {
            alarmDao.deleteAlarm(id);
        });
    }

    public void deleteTable() {
        LocalDatabase.databaseWriteExecutor.execute(() -> {
            alarmDao.deleteTable();
        });
    }

}
