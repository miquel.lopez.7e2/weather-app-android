package com.itb.weatheralarm.mathquizgame.model;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private List<Question> questions;
    private int totalQuestions;
    private Question currentQuestion;

    public Game() {
        totalQuestions = 0;
        currentQuestion = new Question(10);
        questions = new ArrayList<Question>();
    }

    public void newQuestion() {
        currentQuestion = new Question(totalQuestions*2+5);
        totalQuestions++;
        questions.add(currentQuestion);
    }

    public boolean checkAnswer(int answer){
        boolean correct = false;
        if (currentQuestion.getAnswer() == answer){
            correct = true;
        }
        return correct;
    }

    //Setter and Getter


    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(Question currentQuestion) {
        this.currentQuestion = currentQuestion;
    }
}
