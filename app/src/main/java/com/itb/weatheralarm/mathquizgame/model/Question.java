package com.itb.weatheralarm.mathquizgame.model;

import java.util.Random;

public class Question {

    private int firstNumber;
    private int secondNumber;
    private int answer;

    private int[] answersArray;

    private int answerPosition;

    private int upperLimit;

    private String questionPhrase;

    public Question(int upperLimit){
        this.upperLimit = upperLimit;
        Random randomNum = new Random();

        this.firstNumber = randomNum.nextInt(upperLimit);
        this.secondNumber = randomNum.nextInt(upperLimit);
        this.answer = this.firstNumber + this.secondNumber;
        this.questionPhrase = firstNumber + "+" + secondNumber + " = ";

        this.answerPosition = randomNum.nextInt(4);
        this.answersArray = new int[] {0,1,2,3};

        this.answersArray[0] = answer + 1;
        this.answersArray[1] = answer + 10;
        this.answersArray[2] = answer - 5;
        this.answersArray[3] = answer - 2;

        this.answersArray = shuffleArray(this.answersArray);

        answersArray[answerPosition] = answer;
    }

    private int[] shuffleArray(int[] answersArray) {
        int index, temp;
        Random randomNumGenerator = new Random();

        for (int i=answersArray.length - 1; i > 0; i--){
            index = randomNumGenerator.nextInt(i+1);
            temp = answersArray[index];
            answersArray[index] = answersArray[i];
            answersArray[i] = temp;
        }
        return answersArray;
    }

    //Getter and Setter

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int[] getAnswersArray() {
        return answersArray;
    }

    public void setAnswersArray(int[] answersArray) {
        this.answersArray = answersArray;
    }

    public int getAnswerPosition() {
        return answerPosition;
    }

    public void setAnswerPosition(int answerPosition) {
        this.answerPosition = answerPosition;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(int upperLimit) {
        this.upperLimit = upperLimit;
    }

    public String getQuestionPhrase() {
        return questionPhrase;
    }

    public void setQuestionPhrase(String questionPhrase) {
        this.questionPhrase = questionPhrase;
    }
}
