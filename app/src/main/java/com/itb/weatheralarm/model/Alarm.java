
package com.itb.weatheralarm.model;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver;
import com.itb.weatheralarm.broadcast.ApiBroadcastReceiver;
import com.itb.weatheralarm.utils.DayUtil;

import java.io.Serializable;
import java.util.Calendar;

import static com.itb.weatheralarm.MyValues.DEFAULT_API_CALL_TIME;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.ALARMID;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.FRIDAY;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.MONDAY;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.RECURRING;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.SATURDAY;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.SUNDAY;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.THURSDAY;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.TUESDAY;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.WEDNESDAY;

/**
 * Classe model de les alarmes, les programa i cancel·la
 */
@Entity(tableName = "alarms")
public class Alarm implements Serializable {

    @PrimaryKey
    public int alarmId;
    public String title, recurringDaysText;
    private int hour, minute, postpone;
    public boolean started, recurring, apiServiceStarted, game, forecast;
    public boolean monday, tuesday, wednesday, thursday, friday, saturday, sunday;


    public Alarm(int alarmId, int hour, int minute, int postpone, String title, boolean started /*boolean apiServiceStarted*/, boolean recurring, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday, boolean saturday, boolean sunday, boolean game, boolean forecast) {
        this.alarmId = alarmId;
        this.hour = hour;
        this.minute = minute;
        this.postpone = postpone;
        this.started = started;
        //this.apiServiceStarted = apiServiceStarted;

        this.recurring = recurring;

        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;

        this.game = game;
        this.forecast = forecast;

        this.title = title;
    }

    public Alarm() {

    }

    @Ignore
    public Alarm(int alarmId) {
        this.alarmId = alarmId;
    }

    public boolean isGame() {
        return game;
    }

    public void setGame(boolean game) {
        this.game = game;
    }

    public boolean isApiServiceStarted() {
        return apiServiceStarted;
    }

    public void setApiServiceStarted(boolean apiServiceStarted) {
        this.apiServiceStarted = apiServiceStarted;
    }

    public boolean isRecurring() {
        return recurring;
    }

    public boolean isMonday() {
        return monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public int getAlarmId() {
        return alarmId;
    }

    public String getTitle() {
        return title;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setAlarmId(int alarmId) {
        this.alarmId = alarmId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    public int getPostpone() {
        return postpone;
    }

    public void setPostpone(int postpone) {
        this.postpone = postpone;
    }

    public boolean isForecast() {
        return forecast;
    }

    public void setForecast(boolean forecast) {
        this.forecast = forecast;
    }

    public void scheduleAlarm(Context context) {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        intent.putExtra(ALARMID, getAlarmId());
        intent.putExtra(RECURRING, recurring);
        intent.putExtra(MONDAY, monday);
        intent.putExtra(TUESDAY, tuesday);
        intent.putExtra(WEDNESDAY, wednesday);
        intent.putExtra(THURSDAY, thursday);
        intent.putExtra(FRIDAY, friday);
        intent.putExtra(SATURDAY, saturday);
        intent.putExtra(SUNDAY, sunday);

        PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        // if alarm time has already passed, increment day by 1
        if (calendar.getTimeInMillis() <= System.currentTimeMillis()) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
        }

        if (!isForecast()) {
            String toastText = String.format("Alarm %s scheduled for %s at %02d:%02d", title, DayUtil.toDay(calendar.get(Calendar.DAY_OF_WEEK)), hour, minute, alarmId);
            Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show();
        }

        if (!recurring) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExact(
                        AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        alarmPendingIntent
                );
            }
        } else {
            final long RUN_DAILY = 24 * 60 * 60 * 1000;
            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(),
                    RUN_DAILY,
                    alarmPendingIntent
            );
        }

    }

    public void cancelAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);
        alarmManager.cancel(alarmPendingIntent);

        String toastText = String.format("Alarm cancelled for %02d:%02d", hour, minute);
        Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show();

    }

    public void cancelApiService(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ApiBroadcastReceiver.class);
        PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);
        alarmManager.cancel(alarmPendingIntent);

        String toastText = String.format("Api service cancelled for %02d:%02d", hour, minute);
        Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show();

    }

    public String getRecurringDaysText() {
        if (!recurring) {
            return null;
        }

        String days = "";
        if (monday) {
            days += "Mo ";
        }
        if (tuesday) {
            days += "Tu ";
        }
        if (wednesday) {
            days += "We ";
        }
        if (thursday) {
            days += "Th ";
        }
        if (friday) {
            days += "Fr ";
        }
        if (saturday) {
            days += "Sa ";
        }
        if (sunday) {
            days += "Su ";
        }

        return days;
    }

    public void setRecurringDaysText(String recurringDaysText) {
        this.recurringDaysText = recurringDaysText;
    }

    public void startApiService(Context context, int alarmId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, ApiBroadcastReceiver.class);
        intent.putExtra(ALARMID, alarmId);

        PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        //It's from testing, here you put the time when you want to trigger ApiBroadcast and service
        int auxHour;
        int auxMinute;
        if (minute - DEFAULT_API_CALL_TIME <= 0) {
            auxHour = hour - 1;
            auxMinute = minute + DEFAULT_API_CALL_TIME;
        } else {
            auxHour = hour;
            auxMinute = minute - DEFAULT_API_CALL_TIME;
        }
        calendar.set(Calendar.HOUR_OF_DAY, auxHour);
        calendar.set(Calendar.MINUTE, auxMinute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);


        if (calendar.getTimeInMillis() <= System.currentTimeMillis()) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
        }

        String toastText = String.format("Alarm %s scheduled for %s at %02d:%02d", title, DayUtil.toDay(calendar.get(Calendar.DAY_OF_WEEK)), hour, minute, alarmId);
        Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show();

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmPendingIntent);

        setApiServiceStarted(true);
    }


}

