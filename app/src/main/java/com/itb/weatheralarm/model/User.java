package com.itb.weatheralarm.model;

import java.util.List;

public class User {

    private String password;
    private String email;
    private List<Alarm> alarms;

    public User(String password, String email) {
        this.password = password;
        this.email = email;
    }

    public User() {
    }

    public List<Alarm> getAlarms() {
        return alarms;
    }

    public void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
