package com.itb.weatheralarm.model.weatherApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Clouds {
    @SerializedName("all")
    @Expose
    private Integer all;
}
