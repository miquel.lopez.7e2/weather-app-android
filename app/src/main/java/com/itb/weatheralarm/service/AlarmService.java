package com.itb.weatheralarm.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Vibrator;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.itb.weatheralarm.AlarmRing;
import com.itb.weatheralarm.AlarmRingNormal;
import com.itb.weatheralarm.R;
import com.itb.weatheralarm.api.WeatherRepository;
import com.itb.weatheralarm.database.local.LocalDatabaseRepository;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.ui.AlarmsViewModel;

import static com.itb.weatheralarm.MyValues.ALARM_CHANNEL_ID;
import static com.itb.weatheralarm.MyValues.ALARM_TITLE;
import static com.itb.weatheralarm.MyValues.DEFAULT_LOCATION;
import static com.itb.weatheralarm.MyValues.DEFAULT_RING_TONE;
import static com.itb.weatheralarm.MyValues.LOCATION;
import static com.itb.weatheralarm.MyValues.POSTPONE;
import static com.itb.weatheralarm.MyValues.SHARED_PREFERENCES;
import static com.itb.weatheralarm.MyValues.TEMPERATURE;
import static com.itb.weatheralarm.MyValues.TONE;
import static com.itb.weatheralarm.MyValues.VIBRATION;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.ALARMID;


public class AlarmService extends Service {

    private LocalDatabaseRepository repositoryDB;
    private WeatherRepository repository;
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;
    private String location;
    private boolean vibrate;
    private double temperature;
    NotificationManager notificationManager;
    AlarmsViewModel mViewModel;
    SharedPreferences preferences;
    int alarmId = 0;


    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        repositoryDB = new LocalDatabaseRepository(getApplication());
        repository = new WeatherRepository();
        mViewModel = new AlarmsViewModel(getApplication());
        String tone = preferences.getString(TONE, DEFAULT_RING_TONE);
        vibrate = preferences.getBoolean(VIBRATION, false);
        mediaPlayer = MediaPlayer.create(this, getResources().getIdentifier(tone, "raw", getPackageName()));
        mediaPlayer.setLooping(true);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Handler mainHandler = new Handler(Looper.getMainLooper());

        AsyncTask.execute(() -> {
            alarmId = intent.getIntExtra(ALARMID, 0);

            Alarm alarm = repositoryDB.getAlarmByID(alarmId);
            location = preferences.getString(LOCATION, DEFAULT_LOCATION);
            temperature = repository.getCurrentWeather(location);

            Runnable myRunnable = () -> {
                //If alarm id is 1, it's snooze alarm so it will be deleted. Else, alarm will be updated
                if (alarm.getAlarmId() < 0) {
                    startAlarm(alarm.getPostpone(), alarm.getTitle(), temperature);
                    mViewModel.deleteAlarm(alarm);
                } else {
                    alarm.setStarted(false);
                    //System.out.println(alarm.getPostpone() + " EL POSTPONE ES ESTE");
                    if (alarm.isGame()) {
                        startGameAlarm();
                    } else {
                        startAlarm(alarm.getPostpone(), alarm.getTitle(), temperature);
                    }
                    mViewModel.updateAlarmStatus(alarm);
                }
            };
            mainHandler.post(myRunnable);

        });

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        mediaPlayer.stop();
        vibrator.cancel();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager = getSystemService(NotificationManager.class);
            NotificationChannel notificationChannel = new NotificationChannel(ALARM_CHANNEL_ID, "Alarm ringing", NotificationManager.IMPORTANCE_HIGH);


            notificationChannel.enableLights(true);
            notificationChannel.setSound(null, null);
            notificationChannel.setLightColor(Color.RED);
            notificationManager.createNotificationChannel(notificationChannel);
        }

    }

    private void startGameAlarm() {

        Intent notificationIntent = new Intent(this, AlarmRing.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = createNotification(pendingIntent);

        mediaPlayer.start();
        long[] pattern = {0, 100, 1000};
        vibrator.vibrate(pattern, 0);

        startForeground(1, notification);


    }

    private Notification createNotification(PendingIntent intent) {

        createNotificationChannel();
        return new NotificationCompat.Builder(this, ALARM_CHANNEL_ID)
                .setAutoCancel(true)
                .setCategory(Notification.CATEGORY_ALARM)
                .setSmallIcon(R.drawable.ic_alarm_add_black_24dp)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(intent)
                .setContentText("ALARM IS RINGING!")
                .setFullScreenIntent(intent, true)
                .build();
    }

    public void startAlarm(int postpone, String title, double temperature) {

        Intent notificationIntent = new Intent(this, AlarmRingNormal.class);
        notificationIntent.putExtra(POSTPONE, postpone);
        notificationIntent.putExtra(ALARM_TITLE, title);
        notificationIntent.putExtra(TEMPERATURE, temperature);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, alarmId, notificationIntent, 0);

        Notification notification = createNotification(pendingIntent);

        mediaPlayer.start();
        long[] pattern = {0, 100, 1000};

        if (vibrate) {
            vibrator.vibrate(pattern, 0);
        }

        startForeground(1, notification);
    }

}
