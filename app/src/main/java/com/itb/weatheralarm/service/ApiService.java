
package com.itb.weatheralarm.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import androidx.annotation.Nullable;

import com.itb.weatheralarm.api.WeatherRepository;
import com.itb.weatheralarm.database.local.LocalDatabaseRepository;
import com.itb.weatheralarm.model.Alarm;

import static com.itb.weatheralarm.MyValues.DEFAULT_LOCATION;
import static com.itb.weatheralarm.MyValues.DEFAULT_RAIN_TIME;
import static com.itb.weatheralarm.MyValues.DEFAULT_SNOW_TIME;
import static com.itb.weatheralarm.MyValues.LOCATION;
import static com.itb.weatheralarm.MyValues.RAIN_TIME_TEXT;
import static com.itb.weatheralarm.MyValues.SHARED_PREFERENCES;
import static com.itb.weatheralarm.MyValues.SNOW_TIME_TEXT;
import static com.itb.weatheralarm.broadcast.AlarmBroadcastReceiver.ALARMID;

public class ApiService extends Service {

    WeatherRepository repository;
    private LocalDatabaseRepository repositoryDB;
    SharedPreferences preferences;
    int alarmId;


    @Override
    public void onCreate() {

        super.onCreate();
        repository = new WeatherRepository();
        repositoryDB = new LocalDatabaseRepository(getApplication());
        preferences = getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Handler mainHandler = new Handler(Looper.getMainLooper());

        AsyncTask.execute(() -> {

            int rainTime, snowTime;
            String location;
            rainTime = preferences.getInt(RAIN_TIME_TEXT, DEFAULT_RAIN_TIME);
            snowTime = preferences.getInt(SNOW_TIME_TEXT, DEFAULT_SNOW_TIME);
            location = preferences.getString(LOCATION, DEFAULT_LOCATION);
            alarmId = intent.getIntExtra(ALARMID, 0);

            //Gets the api call result
            int delayInMinutes = repository.getDelay(location, rainTime, snowTime);

            // Schedule the alarm with delay time
            Runnable myRunnable = () -> scheduleAlarm(alarmId, delayInMinutes);
            mainHandler.post(myRunnable);
        });

        return START_STICKY;
    }

    private void scheduleAlarm(int alarmId, int delayInMinutes) {

        Alarm alarm = repositoryDB.getAlarmByID(alarmId);
        alarm.setApiServiceStarted(false);
        alarm.setMinute(alarm.getMinute() - delayInMinutes);
        alarm.scheduleAlarm(getApplicationContext());
        System.out.println("ALARM SCHEDULED");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
