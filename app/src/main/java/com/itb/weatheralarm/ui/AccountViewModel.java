package com.itb.weatheralarm.ui;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.itb.weatheralarm.database.firestore.FirebaseAuthentication;
import com.itb.weatheralarm.database.local.LocalDatabaseRepository;

//View Model per a tots els fragments del Compte de l'usuari

public class AccountViewModel extends AndroidViewModel {

    LocalDatabaseRepository repository;
    FirebaseAuthentication mAuth;
    Context context;

    public AccountViewModel(@NonNull Application application) {
        super(application);
        context = application.getApplicationContext();
        this.mAuth = new FirebaseAuthentication();
        this.repository = new LocalDatabaseRepository(application);
    }

    public void createUser(String email, String pass) {
        mAuth.createUser(context, email, pass);
    }


    public void logOut() {
        mAuth.signOut();
    }

    public void deleteTable() {
        new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                }
            }
        }.start();
        repository.deleteTable();
    }

    public void changePassword(String oldPass, String password) {
        mAuth.changePassword(context, oldPass, password);
    }

    public void updateEmail(String email, String password) {
        mAuth.updateEmail(context, email, password);
    }

    public void forgotPass(String email) {
        mAuth.restorePassword(email);
    }

}
