package com.itb.weatheralarm.ui;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.database.firestore.FirestoreDatabase;
import com.itb.weatheralarm.database.local.LocalDatabaseRepository;
import com.itb.weatheralarm.model.Alarm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

//View Model per a tots els fragments d'Alarma

public class AlarmsViewModel extends AndroidViewModel {

    private LocalDatabaseRepository repository;
    private FirestoreDatabase db;
    private String tone = "ring";
    private boolean isUpdate = false;
    private MutableLiveData<List<String>> songList = new MutableLiveData<>();

    public AlarmsViewModel(@NonNull Application application) {
        super(application);
        repository = new LocalDatabaseRepository(application);
        db = new FirestoreDatabase();
    }

    public LiveData<List<Alarm>> getAlarms() {
        return repository.getAlarms();
    }

    public void updateAlarmStatus(Alarm alarm) {
        repository.update(alarm);
        db.updateAlarm(alarm);
    }

    public void insertAlarm(Alarm alarm) {
        repository.insert(alarm);
        db.uploadAlarm(alarm);
    }

    public MutableLiveData<List<String>> getSongList() {
        List<String> songs = new ArrayList<>();
        Field[] field = R.raw.class.getFields();
        for (int i=0; i < field.length; i++){
            songs.add(field[i].getName());
        }
        songList.setValue(songs);
        return songList;
    }

    public void insertAlarmRoom(Alarm alarm) {
        repository.insert(alarm);
    }

    public LiveData<Alarm> getAlarmByIDStream(int id) {
        return repository.getAlarmByIDStream(id);
    }

    public void deleteAlarm(Alarm alarm) {
        repository.deleteAlarm(alarm.getAlarmId());
        db.deleteAlarm(alarm);
    }

    public boolean isUpdate(int id) {
        isUpdate = id != -1;
        return isUpdate;
    }

    public LiveData<List<Alarm>> getAlarmsFromFirebase() {
        new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                }
            }
        }.start();
        return db.getAlarmsFromDB();
    }

}