package com.itb.weatheralarm.ui.addalarm;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputEditText;
import com.itb.weatheralarm.R;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.ui.AlarmsViewModel;
import com.itb.weatheralarm.utils.TimePickerUtil;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.itb.weatheralarm.MyValues.DEFAULT_CREATE_ID;
import static com.itb.weatheralarm.MyValues.DEFAULT_POSTPONE_TIME;

public class AddAlarmFragment extends Fragment {

    private AlarmsViewModel mViewModel;

    private int id;

    @BindView(R.id.timePicker)
    TimePicker timePicker;
    @BindView(R.id.etAlarmName)
    TextInputEditText etName;
    @BindView(R.id.recurringCheckbox)
    CheckBox recurring;
    @BindView(R.id.monCheckbox)
    CheckBox mon;
    @BindView(R.id.tueCheckbox)
    CheckBox tue;
    @BindView(R.id.wedCheckbox)
    CheckBox wed;
    @BindView(R.id.thuCheckbox)
    CheckBox thu;
    @BindView(R.id.friCheckbox)
    CheckBox fri;
    @BindView(R.id.satCheckbox)
    CheckBox sat;
    @BindView(R.id.sunCheckbox)
    CheckBox sun;
    @BindView(R.id.gameCheckbox)
    CheckBox game;
    @BindView(R.id.checkForecast)
    CheckBox forecast;

    @BindView(R.id.recurringOptions)
    LinearLayout recurringOptions;
    @BindView(R.id.tvPostponeMin)
    TextView tvPosponeMin;

    LiveData<Alarm> alarmUpdate;
    private int postpone;

    public static AddAlarmFragment newInstance() {
        return new AddAlarmFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.add_alarm_fragment, container, false);
        ButterKnife.bind(this, view);
        postpone = DEFAULT_POSTPONE_TIME;
        recurring.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                recurringOptions.setVisibility(View.VISIBLE);
            } else {
                recurringOptions.setVisibility(View.GONE);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            id = AddAlarmFragmentArgs.fromBundle(getArguments()).getAlarm();
        }

    }


    private void scheduleAlarm() {

        int alarmId = new Random().nextInt(Integer.MAX_VALUE);
        String title;
        if (etName.getText().toString().isEmpty()) {
            title = "My alarm";
        } else {
            title = etName.getText().toString();
        }

       /* if (!forecast.isChecked()) {
            isStarted = true;
        }*/
        Alarm alarm = new Alarm(
                alarmId,
                TimePickerUtil.getTimePickerHour(timePicker),
                TimePickerUtil.getTimePickerMinute(timePicker),
                postpone,
                title,
                true
                /*forecast.isChecked()*/,
                recurring.isChecked(),
                mon.isChecked(),
                tue.isChecked(),
                wed.isChecked(),
                thu.isChecked(),
                fri.isChecked(),
                sat.isChecked(),
                sun.isChecked(),
                game.isChecked(),
                forecast.isChecked()
        );

        //If id from alarm is -1,  it means that the alarm is being created. If not, it means alarm is getting updated

        if (id == DEFAULT_CREATE_ID) {
            //Save alarm in database
            mViewModel.insertAlarm(alarm);

            if (forecast.isChecked()) {
                setAlarm(alarm);
            } else {
                alarm.scheduleAlarm(getContext());
            }

        } else {

            alarm.setAlarmId(id);
            mViewModel.updateAlarmStatus(alarm);
            if (alarmUpdate.getValue().isStarted()) {
                alarmUpdate.getValue().cancelAlarm(getContext());
            }

            if (forecast.isChecked()) {
                setAlarmUpdate(alarm);
            } else {
                alarm.scheduleAlarm(getContext());
            }
        }

    }

    public void showAlertWeatherAlarm() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Caution!");
        alert.setMessage("Once the weather alarm is activated, it cannot be turned off until 30 minutes left for it to sound and can be edited 30 minutes before it sounds");
        alert.setPositiveButton("I understood!", (dialog, whichButton) -> {
            Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_alarmlist);
        });

        alert.show();
    }

    private void setAlarm(Alarm alarm) {
        mViewModel.insertAlarm(alarm);
        alarm.startApiService(getContext(), alarm.getAlarmId());
    }

    private void setAlarmUpdate(Alarm alarm) {
        alarm.startApiService(getContext(), alarm.getAlarmId());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(AlarmsViewModel.class);

        if (mViewModel.isUpdate(id)) {
            alarmUpdate = mViewModel.getAlarmByIDStream(id);
            alarmUpdate.observe(getViewLifecycleOwner(), this::setTextViews);
        }

    }

    /**
     * Set the data from the alarm for updating
     *
     * @param alarm Alarm from setting data
     */

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setTextViews(Alarm alarm) {
        timePicker.setHour(alarm.getHour());
        timePicker.setMinute(alarm.getMinute());
        etName.setText(alarm.getTitle());
        recurring.setChecked(alarm.isRecurring());
        mon.setChecked(alarm.isMonday());
        tue.setChecked(alarm.isTuesday());
        wed.setChecked(alarm.isWednesday());
        thu.setChecked(alarm.isThursday());
        fri.setChecked(alarm.isFriday());
        sat.setChecked(alarm.isSaturday());
        sun.setChecked(alarm.isSunday());
        game.setChecked(alarm.isGame());
        forecast.setChecked(alarm.isForecast());
        tvPosponeMin.setText(alarm.getPostpone() + " min");

    }


    /**
     * Show an AlertDialog who asks you how long you want to postpone the alarm and save the value
     */

    @OnClick(R.id.relLayoutPostponeTime)
    public void changePostponeMinutes() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Choose how much time you want to postpose");
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        alert.setView(input);
        alert.setPositiveButton("Ok", (dialog, whichButton) -> {
            String min = input.getText().toString() + " min";
            if (input.getText().toString().isEmpty()) {
                min = "10 min";
            } else {
                postpone = Integer.parseInt(input.getText().toString());
            }
            tvPosponeMin.setText(min);

        });
        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
            //Put actions for CANCEL button here, or leave in blank
        });
        alert.show();
    }


    @OnClick(R.id.save_fab)
    public void onViewClicked() {
        scheduleAlarm();
        if (forecast.isChecked()) {
            showAlertWeatherAlarm();
        } else {
            Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_alarmlist);
        }

    }


}
