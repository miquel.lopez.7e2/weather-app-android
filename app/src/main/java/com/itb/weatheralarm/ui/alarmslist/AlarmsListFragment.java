package com.itb.weatheralarm.ui.alarmslist;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.adapter.AlarmAdapter;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.ui.AlarmsViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmsListFragment extends Fragment {

    @BindView(R.id.rv_alarmlist)
    RecyclerView recyclerView;

    @BindView(R.id.ifEmpty)
    TextView ifEmpty;

    private ProgressDialog dialog;
    private AlarmsViewModel alarmsViewModel;

    private LiveData<List<Alarm>> alarms;

    private LiveData<List<Alarm>> alarmsFromFirebase;
    private boolean isLogin = false;

    AlarmAdapter alarmAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_alarmlist, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            isLogin = AlarmsListFragmentArgs.fromBundle(getArguments()).getIslogin();
        }

        //RECYCLERVIEW
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        alarmsViewModel = ViewModelProviders.of(getActivity()).get(AlarmsViewModel.class);
        alarmAdapter = new AlarmAdapter(this.getActivity().getApplication());
        alarmAdapter.setAlarmListener(this::viewAlarmClicked);
        alarmAdapter.setToggleListener(this::onToggleChanged);
        recyclerView.setAdapter(alarmAdapter);

        if (isLogin) {
            dialog = ProgressDialog.show(getContext(), "Loading", "Wait a moment", true);
            syncDataFromFirebase();
            observeAlarms();
        } else {
            observeAlarms();
        }


    }

    private void observeAlarms() {
        alarms = alarmsViewModel.getAlarms();
        alarms.observe(getViewLifecycleOwner(), this::alarmsChanged);
    }

    private void syncDataFromFirebase() {

        alarmsFromFirebase = alarmsViewModel.getAlarmsFromFirebase();
        alarmsFromFirebase.observe(getViewLifecycleOwner(), this::alarmsFromFirebase);

    }

    private void alarmsFromFirebase(List<Alarm> alarms) {

        LiveData<List<Alarm>> aux;
        aux = alarmsViewModel.getAlarms();
        aux.observe(this, this::alarmsArrived);
    }

    private void alarmsArrived(List<Alarm> alarms) {

        for (int i = 0; i < alarmsFromFirebase.getValue().size(); i++) {
            alarmsFromFirebase.getValue().get(i).setStarted(false);
            alarmsViewModel.insertAlarmRoom(alarmsFromFirebase.getValue().get(i));

        }

        dialog.dismiss();
    }

    public boolean onBackPressed() {
        return false;
    }

    private void onToggleChanged(Alarm alarm) {

        if (alarm.isStarted()) {
            alarm.cancelAlarm(getContext());
            alarm.setStarted(false);
            alarmsViewModel.updateAlarmStatus(alarm);
        } else {
            alarm.scheduleAlarm(getContext());
            alarm.setStarted(true);
            alarmsViewModel.updateAlarmStatus(alarm);
        }

    }

    private void alarmsChanged(List<Alarm> alarms) {
        alarmAdapter.setAlarms(alarms);

        if (alarms.isEmpty()) {
            ifEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            ifEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void viewAlarmClicked(Alarm alarm) {

        int id = alarm.getAlarmId();
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(AlarmsListFragmentDirections.actionNavigationAlarmlistToNavigationAddAlarm().setAlarm(id));
    }


    //    @OnClick(R.id.add_button)
//    public void onViewClicked() {
//        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(AlarmsListFragmentDirections.actionNavigationAlarmlistToNavigationAddAlarm().setAlarm(DEFAULT_CREATE_ID));
//    }
}