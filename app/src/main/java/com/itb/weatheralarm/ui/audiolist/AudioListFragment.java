package com.itb.weatheralarm.ui.audiolist;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.adapter.AudioAdapter;
import com.itb.weatheralarm.ui.AlarmsViewModel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.itb.weatheralarm.MyValues.LOCATION;
import static com.itb.weatheralarm.MyValues.SHARED_PREFERENCES;
import static com.itb.weatheralarm.MyValues.TONE;

public class AudioListFragment extends Fragment {

    @BindView(R.id.audio_recyclerview)
    public RecyclerView audioRecyclerview;
    public AudioAdapter adapter;
    public AlarmsViewModel mViewModel;
    SharedPreferences preferences;

    public static AudioListFragment newInstance() {
        return new AudioListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.audio_list_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences= getActivity().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(AlarmsViewModel.class);
        // TODO: Use the ViewModel
        audioRecyclerview.setHasFixedSize(true);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this.getActivity());
        audioRecyclerview.setLayoutManager(linearLayout);

        adapter = new AudioAdapter(getActivity().getApplication());
        audioRecyclerview.setAdapter(adapter);
        LiveData<List<String>> songs = mViewModel.getSongList();
        songs.observe(this, this::onSongsChanged);
        adapter.setListener(this::onRowClicked);

    }

    private void onRowClicked(String string) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TONE, string);
        editor.apply();
        //mViewModel.setTone(string);
//        Navigation.findNavController(getView()).popBackStack();
        Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.navigation_settings);
    }

    private void onSongsChanged(List<String> strings) {
        adapter.setSongs(strings);
    }

}
