package com.itb.weatheralarm.ui.forgotPassword;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.weatheralarm.R;
import com.itb.weatheralarm.ui.AccountViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.util.Patterns.EMAIL_ADDRESS;

public class ForgotPassFragment extends Fragment {

    private AccountViewModel mViewModel;
    private String email;

    @BindView(R.id.tilForgotPass)
    TextInputLayout tilForgotPass;

    @BindView(R.id.etForgotPass)
    TextInputEditText etForgotPass;

    public static ForgotPassFragment newInstance() {
        return new ForgotPassFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_pass_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    public boolean validate() {
        if (voidText(etForgotPass.getText().toString()) || !EMAIL_ADDRESS.matcher(etForgotPass.getText().toString()).matches()) {
            tilForgotPass.setError("Invalid email format");
            return false;
        } else {
            email = etForgotPass.getText().toString();
        }
        return true;
    }

    private boolean voidText(String text) {
        return text.isEmpty();
    }

    @OnClick(R.id.btnRestore)
    public void restoreClicked() {
        if (validate()) {
            mViewModel.forgotPass(email);
            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_login);
        }

    }

    @OnClick(R.id.btnBackLogin)
    public void backLoginClicked() {
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_login);
    }

    @OnClick(R.id.btnRegister)
    public void registerClicked() {
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_register);
    }


}
