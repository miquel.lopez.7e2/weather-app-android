package com.itb.weatheralarm.ui.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.itb.weatheralarm.R;
import com.itb.weatheralarm.ui.AccountViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.util.Patterns.EMAIL_ADDRESS;

public class LoginFragment extends Fragment {

    private AccountViewModel mViewModel;

    @BindView(R.id.ed_login_email)
    TextInputEditText email;

    @BindView(R.id.ed_login_password)
    TextInputEditText password;

    @BindView(R.id.layoutInputEditTextEmailLogin)
    TextInputLayout emailLayout;

    @BindView(R.id.layoutInputEditTextPasswordLogin)
    TextInputLayout passwordLayout;

    private String emailStr;

    private String passwordStr;

    private FirebaseAuth mAuth;

    private ProgressDialog dialog;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    public boolean onBackPressed(){
        return false;
    }

    @OnClick(R.id.btn_login_login)
    public void loginClicked() {

        if (validate()) {
            dialog = ProgressDialog.show(getContext(), "Loading", "Wait a moment", true);
            loginUser();
        }
    }

    @OnClick(R.id.btn_login_register)
    public void registerClicked() {
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_register);

    }

    private boolean validate() {
        boolean isValid = true;
        emailLayout.setErrorEnabled(false);
        passwordLayout.setErrorEnabled(false);
        if (email.getText().toString().isEmpty() || !EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            emailLayout.setError("Introduce a valid email");
            isValid = false;
        } else {
            emailStr = email.getText().toString();
        }

        if (password.getText().toString().isEmpty()) {
            passwordLayout.setError("Introduce a password");
            isValid = false;
        } else {
            passwordStr = password.getText().toString();
        }

        return isValid;
    }


    private void loginUser() {

        mAuth.signInWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(LoginFragmentDirections.actionNavigationLoginToNavigationAlarmlist().setIslogin(true));
            } else {
                Toast.makeText(getContext(), "Cannot login. Incorrect email or password ", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.dismiss();
    }

    @OnClick(R.id.btn_forgotpassword_login)
    public void forgotPasswordClicked() {
        dialog = ProgressDialog.show(getContext(), "Loading", "Wait a moment", true);
        new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                }
                dialog.dismiss();
            }
        }.start();
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_forgot_password);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mAuth.getCurrentUser() != null) {
            Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_alarmlist);
        }
    }
}
