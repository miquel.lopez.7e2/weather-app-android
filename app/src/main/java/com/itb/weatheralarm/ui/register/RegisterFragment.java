package com.itb.weatheralarm.ui.register;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.itb.weatheralarm.R;
import com.itb.weatheralarm.ui.AccountViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.util.Patterns.EMAIL_ADDRESS;

public class RegisterFragment extends Fragment {

    private AccountViewModel mViewModel;

    //edit texts
    @BindView(R.id.ed_register_email)
    TextInputEditText email;
    @BindView(R.id.ed_register_password)
    TextInputEditText password;
    @BindView(R.id.ed_register_password_repeat)
    TextInputEditText passwordReapeted;

    //layout
    @BindView(R.id.layoutInputEditTextPasswordRepeatRegister)
    TextInputLayout layoutPasswordReapeted;
    @BindView(R.id.layoutInputEditTextEmailRegister)
    TextInputLayout layoutEmail;
    @BindView(R.id.layoutInputEditTextPasswordRegister)
    TextInputLayout layoutPassword;

    //Friebase
    private FirebaseAuth mAuth;


    private String emailStr;
    private String passwordStr;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.btn_register_register)
    public void register() {

        if (validate()) {
            mViewModel.createUser(emailStr, passwordStr);
            loginUser();
        }
    }

    private void loginUser() {

        mAuth.signInWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(RegisterFragmentDirections.actionNavigationRegisterToNavigationAlarmlist().setIslogin(true));
            } else {
                Toast.makeText(getContext(), "Cannot login. Incorrect email or password ", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @OnClick(R.id.btn_register_login)
    public void goToLogin() {
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_login);
    }

    public boolean validate() {
        boolean valide = true;
        layoutEmail.setErrorEnabled(false);
        layoutPassword.setErrorEnabled(false);
        layoutPasswordReapeted.setErrorEnabled(false);

        if (voidText(email.getText().toString()) || !EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            valide = false;
            layoutEmail.setError("Invalid email format");
        } else {
            emailStr = email.getText().toString();
        }

        if (password.getText().toString().isEmpty()) {
            valide = false;
            layoutPassword.setError("Introduce your password");
        }

        if (passwordReapeted.getText().toString().isEmpty()) {
            valide = false;
            layoutPasswordReapeted.setError("Introduce your password again");
        }

        if (!password.getText().toString().isEmpty() && !passwordReapeted.getText().toString().isEmpty() && !password.getText().toString().equals(passwordReapeted.getText().toString())) {
            valide = false;
            layoutPasswordReapeted.setError("Different passwords");
        } else {
            passwordStr = password.getText().toString();
        }

        return valide;
    }

    private boolean voidText(String text) {
        return text.isEmpty();
    }

}
