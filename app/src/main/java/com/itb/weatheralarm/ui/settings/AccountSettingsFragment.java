package com.itb.weatheralarm.ui.settings;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.weatheralarm.R;
import com.itb.weatheralarm.ui.AccountViewModel;

import static android.util.Patterns.EMAIL_ADDRESS;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountSettingsFragment extends Fragment {

    private AccountViewModel mViewModel;
    private String email, password;
    private String oldPassword, newPassword;
    private ProgressDialog dialog;

    @BindView(R.id.etEmailSettings)
    TextInputEditText etEmail;

    @BindView(R.id.etOldPasswordSettings)
    TextInputEditText etOldPass;

    @BindView(R.id.etPasswordSettings)
    TextInputEditText etPass;

    @BindView(R.id.etRepeatSettings)
    TextInputEditText etRepeat;

    @BindView(R.id.etPasswordEmailSettings)
    TextInputEditText etPassEmail;

    @BindView(R.id.tilEmailSettings)
    TextInputLayout tilEmail;

    @BindView(R.id.tilOldPasswordSettings)
    TextInputLayout tilOldPass;

    @BindView(R.id.tilPasswordSettings)
    TextInputLayout tilPass;

    @BindView(R.id.tilRepeatSettings)
    TextInputLayout tilRepeat;

    @BindView(R.id.tilPasswordEmailSettings)
    TextInputLayout tilPassEmail;

    public static AccountSettingsFragment newInstance(int page, String title) {
        AccountSettingsFragment frag = new AccountSettingsFragment();
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_settings_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(getActivity()).get(AccountViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnSaveEmail)
    public void saveEmail() {
        if (validateEmail()) {
            mViewModel.updateEmail(email, password);
        }
    }

    public boolean validateEmail() {
        if (voidText(etEmail.getText().toString()) || !EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            tilEmail.setError("Invalid email format");
            return false;
        } else {
            email = etEmail.getText().toString();
        }

        if (voidText(etPassEmail.getText().toString())) {
            tilPassEmail.setError("Password can not be empty");
            return false;
        } else
            password = etPassEmail.getText().toString();
        return true;
    }

    private boolean voidText(String text) {
        return text.isEmpty();
    }


    @OnClick(R.id.btnSavePass)
    public void savePassword() {
        if (validatePasswords()) {
            mViewModel.changePassword(oldPassword, newPassword);
        }
    }

    private boolean validatePasswords() {
        if (voidText(etPass.getText().toString()) || !etPass.getText().toString().equals(etRepeat.getText().toString())) {
            tilPass.setError("Passwords are not equal or empty");
            return false;
        } else {
            oldPassword = etOldPass.getText().toString();
            newPassword = etPass.getText().toString();
        }

        return true;
    }

    @OnClick(R.id.btnLogout)
    public void logOut() {
        dialog = ProgressDialog.show(getContext(), "Loading", "Wait a moment", true);
        mViewModel.logOut();
        mViewModel.deleteTable();
        closeDialog();
        Navigation.findNavController(this.getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_login);
    }

    public void closeDialog(){
        dialog.dismiss();
    }
}
