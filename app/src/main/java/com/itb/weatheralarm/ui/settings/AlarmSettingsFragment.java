package com.itb.weatheralarm.ui.settings;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.itb.weatheralarm.R;
import com.itb.weatheralarm.model.Alarm;
import com.itb.weatheralarm.ui.AlarmsViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.itb.weatheralarm.MyValues.LOCATION;
import static com.itb.weatheralarm.MyValues.RAIN_TIME_TEXT;
import static com.itb.weatheralarm.MyValues.SHARED_PREFERENCES;
import static com.itb.weatheralarm.MyValues.SNOW_TIME_TEXT;
import static com.itb.weatheralarm.MyValues.VIBRATION;

public class AlarmSettingsFragment extends Fragment {

    @BindView(R.id.textViewRainMinutes)
    TextView rainMinutes;
    @BindView(R.id.textViewSnowMinutes)
    TextView snowMinutes;

    @BindView(R.id.tvCity)
    TextView tvLocation;

    @BindView(R.id.checkVibration)
    CheckBox checkVibration;

    private boolean readStoragePermission = false;
    private AlarmsViewModel mViewModel;
    private LiveData<List<Alarm>> alarmsFromFirebase;
    SharedPreferences preferences;

    ProgressDialog dialog;

    public static AlarmSettingsFragment newInstance(int page, String title) {
        AlarmSettingsFragment frag = new AlarmSettingsFragment();
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.alarm_settings_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(AlarmsViewModel.class);
        preferences = getActivity().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        boolean vibrate = preferences.getBoolean(VIBRATION,false);
        checkVibration.setChecked(vibrate);

        checkVibration.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(VIBRATION, true);
                editor.apply();
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(VIBRATION, false);
                editor.apply();
            }

        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnChangeRain)
    public void changeRainMinutes() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Choose how much time you want to postpose when its raining (max 25 min)");
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        alert.setView(input);
        alert.setPositiveButton("Ok", (dialog, whichButton) -> {
            String min = input.getText().toString() + " minutes";
            if (min.equals("")) {
                min = "0 minutes";
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(RAIN_TIME_TEXT, Integer.parseInt(input.getText().toString()));
                editor.apply();
            }
            rainMinutes.setText(min);

        });
        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {

        });
        alert.show();
    }

    @OnClick(R.id.btnChangeSnow)
    public void changeSnowMinutes() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Choose how much time you want to postpose when its snowing (max 25 min)");
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        alert.setView(input);
        alert.setPositiveButton("Ok", (dialog, whichButton) -> {
            String min = input.getText().toString() + " minutes";
            if (min.isEmpty()) {
                min = "0 minutes";
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(SNOW_TIME_TEXT, Integer.parseInt(input.getText().toString()));
                editor.apply();
            }
            snowMinutes.setText(min);

        });
        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
            //Put actions for CANCEL button here, or leave in blank
        });
        alert.show();
    }

    @OnClick(R.id.btnLocation)
    public void changeLocation() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("In which city do you live?");
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setRawInputType(Configuration.KEYBOARD_NOKEYS);
        alert.setView(input);
        alert.setPositiveButton("Ok", (dialog, whichButton) -> {
            String city = input.getText().toString();
            if (city.isEmpty()) {
                city = "Barcelona";
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(LOCATION, input.getText().toString());
                editor.apply();
            }
            tvLocation.setText(city);

        });
        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
            //Put actions for CANCEL button here, or leave in blank
        });
        alert.show();
    }


    @OnClick(R.id.btnSync)
    public void syncData() {
        dialog = ProgressDialog.show(getContext(), "Loading", "Wait a moment", true);

        alarmsFromFirebase = mViewModel.getAlarmsFromFirebase();
        alarmsFromFirebase.observe(this, this::alarmsFromFirebase);


    }


    private void alarmsFromFirebase(List<Alarm> alarms) {

        LiveData<List<Alarm>> aux;
        aux = mViewModel.getAlarms();
        aux.observe(this, this::alarmsArrived);
        dialog.dismiss();
    }

    private void alarmsArrived(List<Alarm> alarms) {

        for (int i = 0; i < alarmsFromFirebase.getValue().size(); i++) {

            alarmsFromFirebase.getValue().get(i).setStarted(false);
            mViewModel.insertAlarmRoom(alarmsFromFirebase.getValue().get(i));

        }


    }

    @OnClick(R.id.btnSelectTone)
    public void onViewClicked() {
        if (readStoragePermission) {
            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_audio_list);
        } else {
            getReadStoragePermission();
        }
    }

    private void getReadStoragePermission() {
        if (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            readStoragePermission = true;
        } else {
            ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        }
    }



}
